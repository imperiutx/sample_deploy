package main

import (
	"fmt"
	"net/http"
)

func main() {
	fmt.Println("running code")
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		fmt.Fprintf(w, "<h1>Welcome to my website!</h1>")
	})

	// http.HandleFunc("/demo", func(w http.ResponseWriter, r *http.Request) {
	// 	fmt.Fprintf(w, "<h1>Welcome to my demo page!</h1>")
	// })

	fs := http.FileServer(http.Dir("static/"))
	http.Handle("/static/", http.StripPrefix("/static/", fs))

	http.ListenAndServe(":80", nil)
}
